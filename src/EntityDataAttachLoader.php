<?php

namespace Drupal\entity_data_attach;


/**
 * Class EntityDataAttachLoader.
 *
 * @package Drupal\entity_data_attach
 */
class EntityDataAttachLoader {

  public $entity;

  public function setEntity($entity) {
    $this->entity = $entity;

    return $this;
  }

  public function get($field) {
    return $this->process($field);
  }

  public function getAll() {
    return $this->process();
  }

  public function process($filter_fieldname = NULL) {
    $entity = $this->entity;
    $results = [];
    $definitions = $entity->getFieldDefinitions();

    $type = \Drupal::service('plugin.manager.entity_data_attach.processor');

    foreach ($definitions as $definition) {
      if ($definition->getType() == 'data_attach') {
        $field_name = $definition->getName();

        if ($filter_fieldname && $filter_fieldname != $field_name) {
          continue;
        }

        $field_values = $entity->{$field_name}->getValue();

        foreach ($field_values as $delta => $field_value) {
          // @TODO this shouldn't be necessary.
          if ($field_value['value'] == DRUPAL_OPTIONAL) {
            continue;
          }
          $instance = $type->createInstance($field_value['value']);

          // Include the correct data attachment.
          $instance->setEntity($entity);
          if (!empty($field_value['config'])) {
            $instance->setConfig($field_value['config']);
          }
          $data = $instance->getData();
          $key = !empty($field_value['key']) ? $field_value['key'] : $field_name;
          $results[$key][$delta] = $data;
        }
      }
    }

    return $results;
  }
}
