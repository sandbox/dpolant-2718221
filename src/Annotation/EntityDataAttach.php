<?php

namespace Drupal\entity_data_attach\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Entity data attach item annotation object.
 *
 * @see \Drupal\entity_data_attach\Plugin\EntityDataAttachManager
 * @see plugin_api
 *
 * @Annotation
 */
class EntityDataAttach extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
