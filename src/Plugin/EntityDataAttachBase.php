<?php

namespace Drupal\entity_data_attach\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Entity data attach plugins.
 */
abstract class EntityDataAttachBase extends PluginBase implements EntityDataAttachInterface {

  private $entity;

  private $config = array();

  // Add common methods and abstract methods for your plugin type here.
  public function getData(){}

  public function setEntity($entity) {
    $this->entity = $entity;
  }

  public function getEntity() {
    return !empty($this->entity) ? $this->entity : NULL;
  }

  public function setConfig(array $config) {
    $this->config = $config;
  }

  public function getConfig() {
    return $this->config;
  }

  public function getDefaultConfigValue($name, $default = '') {
    $config = $this->getConfig();
    return isset($config[$name]) ? $config[$name] : $default;
  }
}
