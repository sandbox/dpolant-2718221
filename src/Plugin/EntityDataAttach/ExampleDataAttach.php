<?php

namespace Drupal\entity_data_attach\Plugin\EntityDataAttach;

use Drupal\entity_data_attach\Plugin\EntityDataAttachBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Example data attach class.
 *
 * @EntityDataAttach(
 *   id = "example_data_attach",
 *   label = @Translation("Example Data attach"),
 *   description = @Translation("Lets you attach data to entities") * )
 */
class ExampleDataAttach extends EntityDataAttachBase {

  public function getData(){
    return array('test' => 'true');
  }

  public function configFormElements(array $form, FormStateInterface $form_state) {
    $elements['test_config_setting'] = array(
      '#type' => 'textfield',
      '#title' => t('Test configuration setting'),
      '#default_value' => $this->getDefaultConfigValue('test_config_setting'),
    );

    return $elements;
  }
}
