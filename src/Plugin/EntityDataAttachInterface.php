<?php

namespace Drupal\entity_data_attach\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Entity data attach plugins.
 */
interface EntityDataAttachInterface extends PluginInspectionInterface {

  // Add get/set methods for your plugin type here.
  public function getData();
}
