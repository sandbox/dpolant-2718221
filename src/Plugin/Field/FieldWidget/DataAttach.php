<?php

namespace Drupal\entity_data_attach\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Component\Utility\NestedArray;

/**
 * Plugin implementation of the 'data_attach' widget.
 *
 * @FieldWidget(
 *   id = "data_attach_default",
 *   label = @Translation("Data attach"),
 *   field_types = {
 *     "data_attach"
 *   }
 * )
 */
class DataAttach extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'size' => 60,
      'placeholder' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $fieldset = $element + array(
      '#type' => 'fieldset',
    );

    $fieldname = $items->getName();
    $target_id = 'data_attach_wrapper-' . $fieldname . '-' . $delta;
    $item_value = $items->getValue() ? $items->getValue()[$delta] : NULL;

    if ($form_state->getValues()) {
      $parents = array_merge($element['#field_parents'], [$fieldname, $delta, 'value']);
      $plugin_name = $form_state->getValue($parents);
    }
    else if (isset($item_value['value'])){
      $plugin_name = $item_value['value'];
    }

    if (isset($plugin_name)) {
      // Add in the plugin's config form if necessary. // @TODO make sure the
      // plugin actually exists.
      $plugin_manager = \Drupal::service('plugin.manager.entity_data_attach.processor');
      $plugin = $plugin_manager->createInstance($plugin_name);
      if (!empty($item_value['config'])) {
        $plugin->setConfig($item_value['config']);
      }

      if ($plugin && method_exists($plugin, 'configFormElements')) {
        $fieldset['config'] = $plugin->configFormElements($form, $form_state);
      }
    }
    else {
      $fieldset['config'] = array(
        '#type' => 'markup',
        '#markup' => '',
      );
    }

    $fieldset['config'] += array(
      '#prefix' => '<div id=' . $target_id . '>',
      '#suffix' => '</div>',
      '#weight' => 10,
      '#tree' => TRUE,
    );

    // @TODO: getting wrong value for #required.
    $fieldset['value'] = array(
      '#type' => 'select',
      '#title' => t('Select attachment'),
      '#default_value' => !empty($item_value) ? $item_value['value'] : NULL,
      '#weight' => 5,
      '#options' => $this->getDataAttachOptions($element['#required']),
      '#ajax' => array(
        'callback' => 'Drupal\entity_data_attach\Plugin\Field\FieldWidget\DataAttach::pluginConfigAjax',
        'wrapper' => $target_id,
      )
    );

    // @TODO: hide this if no plugin is selected. Have to switch AJAX to command
    // mode though.
    $fieldset['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Key'),
      '#weight' => 15,
      '#default_value' => isset($item_value['key']) ? $item_value['key'] : NULL,
      '#description' => t('Optionally specify a key to use in the entity data structure to attach this data.')
    );

    return $fieldset;
  }

  public function getDataAttachOptions($required = FALSE) {
    $options = $required ? [] : [DRUPAL_OPTIONAL => t('- None - ')];
    foreach (entity_data_attach_get_definitions() as $definition) {
      $id = $definition['id'];
      $options[$id] = $definition['label'];
    }

    return $options;
  }

  public function pluginConfigAjax(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    $parents[] = 'config';
    $element = NestedArray::getValue($form, $parents);

    return $element;
  }
}
