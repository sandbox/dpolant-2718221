<?php

namespace Drupal\entity_data_attach\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Entity data attach plugin manager.
 */
class EntityDataAttachManager extends DefaultPluginManager {

  /**
   * Constructor for EntityDataAttachManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EntityDataAttach', $namespaces, $module_handler, 'Drupal\entity_data_attach\Plugin\EntityDataAttachInterface', 'Drupal\entity_data_attach\Annotation\EntityDataAttach');

    $this->alterInfo('entity_data_attach_entity_data_attach_info');
    $this->setCacheBackend($cache_backend, 'entity_data_attach_entity_data_attach_plugins');
  }

}
